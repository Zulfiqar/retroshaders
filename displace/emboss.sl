/*listing 8.3 from Advanced Renderman, pg. 197*/
/*the displace function is in the displace.h*/
/*header file*/

#include "../include/displace.h"

displacement emboss (
	string texturename = "";
	float Km = 1;
	string dispspace = "shader";
	float truedisp = 1;
	float sstart = 0, sscale = 1;
	float tstart = 0, tscale = 1;
	float blur = 0;
)
{
	if (texturename != "") {
		float ss = (s - sstart) / sscale;
		float tt = (t - tstart) / tscale;
		float amp = float texture (texturename[0], ss , tt, "blur", blur);
		N =  Displace (normalize(N), dispspace, -Km * amp, truedisp);
	}
}
