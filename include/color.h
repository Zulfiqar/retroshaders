/* needs projection.h */

color GetColorTextureAndAlpha (
		string texturename;
		string projection;
		point pt;
		string whichspace;
		matrix xform;
		float blur;
		float alphachannel;
		output float alpha;
		)
{
	float ss, tt, ds, dt;
	planar_projection(projection, pt, whichspace, xform, ss, tt, ds, dt);
	ds *= 0.5;
	dt *= 0.5;
	color Ct = color texture(texturename, ss-ds, tt-dt, ss+ds, tt-dt,
			ss-ds, tt+dt, ss+ds, tt+dt, "blur", blur);
	alpha = float texture(texturename[alphachannel], ss-ds, tt-dt,
				ss+ds, tt-dt, ss-ds, tt+dt, ss+ds, tt+dt,
			       	"blur", blur, "fill", 1);
	return Ct;
}

color ApplyColorTextureOver (
		color basecolor;
		string texturename;
		string projection;
		point pt;
		string whichspace;
		matrix xform;
		float blur;
		)
{
	float alpha;
	color Ct = GetColorTextureAndAlpha (texturename, projection, pt, whichspace, xform, blur, 3, alpha);
	return Ct + (1 -alpha)*basecolor;
}


float ApplyFloatTextureOver (
		string texturename;
		string projection;
		point pt;
		string whichspace;
		matrix xform;
		float blur;
		)
{
	float alpha;
	float val;
	color Ct = GetColorTextureAndAlpha (texturename, projection, pt, whichspace, xform, blur, 3, alpha);
	val = comp(Ct, 0);
	/* K-value get's scaled by the alphachannel and the greyvalue of the red channel*/
	val = clamp(val, 0, 1);
	return val*(1 -alpha);
}

float ConvertToGrey (
		string texturename;
		string projection;
		point pt;
		string whichspace;
		matrix xform;
		float blur;
		)
{
	float alpha;
	float gval;
	color Ct = GetColorTextureAndAlpha (texturename, projection, 
			pt, whichspace,	xform, blur, 3, alpha);
	float cr = comp(Ct, 0);
	float cg = comp(Ct, 1);
	float cb = comp(Ct, 2);
	if (cr != cg && cr != cb) 
		gval = (0.211 * cr + 0.711 * cg + 0.078 * cb) * (1 -alpha);
	 else 
		gval = cr * (1 -alpha);
	
	return gval;
}
