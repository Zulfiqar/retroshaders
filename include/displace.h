/*displacement functions*/

vector Displace ( vector dir; string space; float amp; int truedisp;)
{
	float spacescale = length(vtransform(space, dir));
	vector Ndisp = dir * (amp / spacescale);
	P += truedisp * Ndisp;
	return normalize (computenormal (P + (1-truedisp)*Ndisp));
}

vector Displacevector (vector dir; string space; float amp; int truedisp; export vector pnt;)
{
	float spacescale = length(vtransform(space, dir));
	vector Ndisp = dir * (amp / spacescale);
	pnt += truedisp * Ndisp;
	return normalize (computenormal (pnt + (1-truedisp)*Ndisp));
}

