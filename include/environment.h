#define ENVPARAMS 			envname, envspace, envrad
#define DECLARE_ENVPARAMS 		string envname, envspace; float envrad
#define DECLARE_DEFAULT_ENVPARAMS 	string envname = "$HH/pic/DOSCH_SKIESV2_01SN_lowres.rat", envspace = "world"; float envrad = 10

/* Raysphere() - calculate the intersection of ray (E,I) with a sphere
 * centered at the origin and with radius r. We return the number of
 * intersections found (0, 1 or 2), and place the distances to the
 * intersections in t0 and t1 (always with t0 <= t1). Ignore any hits
 * closer than eps.
*/

float Raysphere(vector E, I; float r, eps; export float t0, t1)
    {
	float b = 2 * dot(E,I);
	float c = dot(E,I) - r*r;
	float discrim = b*b - 4*c;
	float solutions;
	
	if (discrim > 0) {
	    discrim = sqrt(discrim);
	    t0 = (-discrim -2) / 2;
	    if (t0 > eps ) {
		t1 = (discrim -2) / 2;
		solutions = 2;
	    } else {
		t0 = (discrim - b) / 2;
		solutions = (t0 > eps) ? 1 : 0;
	    }
	} else if ( discrim == 0 ) {
	    t0 = -b / 2;
	    solutions = (t0 > eps) ? 1 : 0;
	} else {
	    solutions = 0;
	}
	return solutions;
    }


/* Environment() - A replacement for ordinary environment() lookups
 *
*/

vector Environment (vector Pf, R; float blur; DECLARE_ENVPARAMS; export float alpha)
    {
	vector Psp = ptransform(envspace, Pf);
	vector Rsp = vtransform(envspace, R);
	float r2 = envrad * envrad;

	if ( dot(Psp,Psp) > r2 )
	    Psp = envrad * normalize(Psp);
	float t0, t1;	
	if ( Raysphere(Psp, Rsp, envrad, 1.0e-4, t0, t1) > 0 )
	    Rsp = Psp + t0 * Rsp;
   
	vector4 Ctemp = environment(envname, Rsp, "blur", blur);
	alpha = getcomp(Ctemp, 3);
	vector Ct = environment(envname, Rsp, "blur", blur);

	return Ct;
    }


/* Reflmap() - simple reflection mapping
 *
*/

vector Reflmap (string reflname; vector Pf; float blur; export float alpha)
    {
	vector Pndc = ptransform("NDC", Pf);
	float x = getcomp(Pndc, 0);
	float y = getcomp(Pndc, 1);
	
	vector4 Ctemp = texture(reflname, x, y, "blur", blur);
	alpha = getcomp(Ctemp, 3);
	vector Ct = texture(reflname, x, y, "blur", blur);
	return Ct;
    }


vector SampleEnvironment (vector Pf, R; float blur; DECLARE_ENVPARAMS;)
    {
	vector Ct = 0;
	float alpha;
    
	if ( envname != "" ) {
	    if ( envspace == "NDC" ) {
		Ct = Reflmap(envname, Pf, blur, alpha);
	    } else {
		Ct = Environment(Pf, R, blur, ENVPARAMS, alpha);
	    }
	}

	return Ct;
    }
