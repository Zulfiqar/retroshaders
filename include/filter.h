#define MINFILTWIDTH	    1.0e-6

float Filterwidth(float t)
    {
	return max((abs(Du(t)) + abs(Dv(t))), MINFILTWIDTH);
    }

float Filterwidtharea(vector Pf)
    {
	return max(pow(area(Pf), 2), MINFILTWIDTH);
    } 
