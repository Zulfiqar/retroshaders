//requires environment.h

vector MaterialMatte(vector Nf, basecolor; float Ka, Kd)
    {
	return basecolor * (Ka * ambient() + Kd * diffuse(Nf));
    }

vector MaterialPlastic(vector Nf, basecolor; float Ka, Kd, Ks, roughness)
    {
	return basecolor * (Ka * ambient() + Kd *diffuse(Nf)) + Ks * specular(Nf, -normalize(I), roughness);
    }

vector MaterialPlasticShiny(vector Pf, Nf, basecolor; float Ka, Kd, Ks, Kr, roughness, ior, blur; DECLARE_ENVPARAMS)
    {
	vector In = normalize(I);
	vector V = -In;
	float fkr, fkt;
	vector R, T;

	fresnel(In, Nf, 1/ior, fkr, fkt, R, T);
	
	vector Ct = fkt * basecolor * (Ka*ambient() + Kd*diffuse(Nf)) + (Ks *fkr) * specular(Nf, V, roughness)
			+  fkr * Kr * SampleEnvironment(Pf, R, blur, ENVPARAMS);
	return Ct;
    }

vector MaterialPlasticThin(vector Nf, Nr, V, basecolor; float Ka, Kd, Ks, Kt, roughness)
    {
	return basecolor * (Ka * ambient() + Kd * diffuse(Nf) + Kt * diffuse(Nr)) + Ks * specular(Nf, V, roughness);
    }

vector MaterialMetalRough(vector Nf, basecolor; float Ka, Kd, Ks, roughness)
    {
	return basecolor * (Ka * ambient() + Kd *diffuse(Nf) + Ks * specular(Nf, -normalize(I), roughness));
    }

vector MaterialMetalShiny(vector Nf, basecolor; float Ka, Kd, Ks, Kr, roughness, blur; DECLARE_ENVPARAMS)
    {
	vector In = normalize(I);
	vector V = -In;
	vector R = reflect(In, Nf);
	
	vector Ct = basecolor * (Ka * ambient() + Kd * diffuse(Nf) + Ks * specular(Nf, V, roughness) + 
				    Kr * SampleEnvironment(P, R, blur, ENVPARAMS));
	return Ct;
    }

vector MaterialCeramicTiles(vector Pf, Nf, Cmortar, Ctile; float intile, Ka, Kdmortar, Kdtile, Ks, roughness,
			    specsharpness, Kr, blur, eta; DECLARE_ENVPARAMS)
    {
	vector basecolor = lerp(Cmortar, Ctile, intile);
	float ks = Ks * intile;
	float kd = lerp(Kdmortar, Kdtile, intile);
	vector In = normalize(I);
	vector V = -In;
	float fkr, fkt;
	vector R, T;
	fresnel(In, Nf, 1/eta, fkr, fkt, R, T);
	fkt = 1 -fkr;
	float kr = fkr * Kr * intile;
	return fkt * basecolor * (Ka * ambient() + kd * diffuse(Nf)) +
		ks * LocGlossy(Pf, Nf, V, roughness/10, specsharpness) +
		kr * SampleEnvironment(Pf, R, blur, ENVPARAMS);
    }

vector MaterialCel(vector Nf, Ct; float Ka, Kd, Ks, roughness, specsharpness)
    {
	vector In = normalize(I);
	vector V = -In;
	return Ct * (Ka * ambient() + Kd * LocCelDiffuse(Nf)) + Ks * LocGlossy(P, Nf, V, roughness/10, specsharpness);
    }
