/* 20-10-10 changed limport to limport (yeah right 04-04-2011) */

vector LocDiffuse(vector Pf, Ns)
    {
	vector Ct = 0.0;
	illuminance(Pf, Ns, M_PI_2) {
	    float nondiff = 0;
	    limport("__nondiffuse", nondiff);
	    Ct += Cl * (1 - nondiff) * dot(Ns, normalize(L));
	}
	
	return Ct;
    }

vector LocSpecular(vector Pf, Ns, V; float roughness)
    {
	vector Ct = 0.0;
	illuminance(Pf, Ns, M_PI_2) {
	    float nonspec = 0;
	    limport("__nonspecular", nonspec);
	    vector H = normalize(normalize(L) + V);
	    Ct += Cl * (1 - nonspec) * pow(max(0, dot(Ns, H)), 1/roughness);
	}
	
	return Ct;
    }

vector LocOrenNayar(vector Pf, Ns, V; float roughness)
    {
	float sigma2 = roughness * roughness;
	float A =  1 -0.5 * sigma2 / (sigma2 + 0.33);
	float B = 0.45 * sigma2 / (sigma2 + 0.09);
	float theta_r = acos(dot(V, Ns));
	vector V_perp_N = normalize(V - Ns * dot(V, Ns));

	vector Ct = 0.0;
	illuminance(Pf, Ns, M_PI_2) {
	    float nondiff = 0;
	    limport("__nondiffuse", nondiff);
	    if (nondiff < 1) {
		vector Ln = normalize(L);
		float cos_theta_i = dot(Ln, Ns);
		float cos_phi_diff = dot(V_perp_N, normalize(Ln - Ns * cos_theta_i));
		float theta_i = acos(cos_theta_i);
		float alpha = max(theta_i, theta_r);
		float beta = min(theta_i, theta_r);
		Ct += (1 - nondiff) * Cl * cos_theta_i * (A + B * max(0, cos_phi_diff) * sin(alpha) * tan(beta));
	    }
	}

	return Ct;
    }

vector LocWardAnisotropic(vector Pf, Ns, V, xdir; float xroughness, yroughness)
    {
	float cos_theta_r = clamp(dot(Ns, V), 0.0001, 1);
	vector X = xdir / xroughness;
	vector Y = cross(Ns, xdir) / yroughness;

	vector Ct = 0.0;
	illuminance(Pf, Ns, M_PI_2) {
	    float nonspec = 0;
	    limport("__nonspecular", nonspec);   //limport???
	    if (nonspec < 1) {
		vector Ln = normalize(L);
		float cos_theta_i = dot(Ln, N);
		if (cos_theta_i > 0.0) {
		    vector H = normalize(V + Ln);
		    float rho = exp(-2 * (pow(dot(X, H), 2) + pow(dot(Y, H), 2)) / (1 + dot(H, Ns)))
				/ sqrt(cos_theta_i * cos_theta_r);
		    Ct += Cl * (1 - nonspec) * cos_theta_i * rho;
		}
	    }
	}
	Ct = Ct / (4 * xroughness * yroughness);
	
	return Ct;
    }

vector LocGlossy(vector Pf, Ns, V; float roughness, sharpness)
    {
	vector Ct = 0.0;
	float w = 0.18 * (1 - sharpness);
	illuminance(Pf, Ns, M_PI_2) {
	    float nonspec = 0;
	    limport("__nonspecular", nonspec);
	    if (nonspec < 1) {
		vector H = normalize(normalize(L) + V);
		Ct += Cl * (1 - nonspec) * smooth(0.72 - w, 0.72 + w, pow(max(0, dot(H, Ns)), 1/roughness));
	    }
	}

	return Ct;
    }

vector LocCelDiffuse(vector Nf)
    {
	vector Ct = 0.0;
	illuminance(P, Nf, M_PI_2) {
	    float nondiff = 0;
	    limport("__nondiffuse", nondiff);
	    if (nondiff < 1) {
		Ct += Cl * ((1 - nondiff) * smooth(0, 0.1, dot(N, normalize(L))));
	    }
	}

	return Ct;
    }
