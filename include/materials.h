vector TileTexture(float tileindex, stile, ttile, ds, dt, edgevary, mottling,
		    speckly, mottlefreq; vector Cbase, Cedge, Cmottle, Cspeck)
    {
	vector C = Cbase;
	float dst = max(ds, dt);
	vector noisep;
	if (mottling > 0) {
	    noisep = mottlefreq * set(stile, ttile, tileindex);
	    float mottle = 0.2 + 0.6 * Fbm(noisep, mottlefreq * dst, 4, 2, 0.65);
	    C = lerp(C, Cmottle, clamp(mottling * mottling, 0, 1));
	}
	if (edgevary > 0) {
	    float sedgeoffset = 0.05 * Fbm(set(stile * 10, ttile * 10, tileindex + 10), 10 * dst, 2, 2, 0.5);
	    float tedgeoffset = 0.05 * Fbm(set(stile * 10, ttile * 10, tileindex -  3), 10 * dst, 2, 2, 0.5);
	    float edgy = 1 - (SmoothPulse(.05, 0.15, 0.85, 0.95, stile + sedgeoffset) *
				SmoothPulse(.05, 0.15, 0.85, 0.95, ttile + tedgeoffset));
	    C = lerp(C, Cedge, edgevary * edgy);
	}
	if (speckly > 0) {
	    float speckfreq = 7;
	    noisep = set(stile * speckfreq, ttile * speckfreq, tileindex + 8);
	    float specky = Filteredsnoise(noisep, speckfreq * dst);
	    specky = smooth(0.55, 0.7, specky);
	    C = lerp(C, Cspeck, speckly * specky);
	}
	return C;  
    }	


float OakTexture(vector Pshad; float dPshad, ringfreq, ringunevenness, grainfreq;
		 float ringnoise, ringnoisefreq, trunkwobble, trunkwobblefreq;
		 float angularwobble, angularwobblefreq, ringy, grainy;)
    {
	vector offset = Vfbm( Pshad * ringnoisefreq, dPshad * ringnoisefreq, 2, 4, 0.5 );
        vector pring = Pshad + ringnoise * offset;
	pring += trunkwobble * snoise( {1.0, 1.0, 0.0} * getcomp( Pshad, 2 ) * trunkwobblefreq );
        float r2 = pow(getcomp( pring, 0 ), 2 ) + pow( getcomp( pring, 1 ), 2 ); 
	float r  = sqrt( r2 ) * ringfreq;
        r += angularwobble * smooth( 0, 5, r ) * snoise( {1.0, 1.0, 0.1} * angularwobblefreq * pring );
    
	float dr = Filterwidth( r );
        r += 0.5 * Filterednoise( r, dr );

	float inring = SmoothPulsetrain( 0.1, 0.55, 0.7, 0.95, 1.0, r );
    
        vector pgrain = Pshad * grainfreq * {1.0, 1.0, 0.5};
	float dpgrain = Filterwidtharea( pgrain );
        float grain = 0.0;
	float amp = 1.0;
        int i;

	for( i = 0; i < 2; i += 1 ) {
	    float grain1valid = 1 - smooth( 0.2, 0.6, dpgrain );
	    if( grain1valid > 0 ) {
		float g = grain1valid * snoise( pgrain );
		g *= ( 0.3 + 0.7 * inring );
		g = pow( clamp( 0.8 - g, 0, 1 ), 2);
		g = grainy * smooth( 0.5, 1, g );
		if( i == 0 )
		    inring *= ( 1 - 0.4 * grain1valid );
		grain = max( grain, g );
	    }
	    pgrain *= 2;
	    dpgrain *= 2;
	    amp *= 0.5;
	}
    
        return lerp( inring * ringy, 1, grain );
    }

vector CelOutline(vector Nf; float outline, width)
    {
	float angle = abs( dot( normalize(Nf), normalize(I) ) );
	float dangle = Filterwidth(angle);
	float border = 1 - FilteredStep(5 * outline, angle/dangle, width);
	Of = lerp(Of, {1.0, 1.0, 1.0}, border);
	return lerp(Cf, {0.0, 0.0, 0.0}, border);
    }
