//Renderman compatibility
vector Cellnoise3D(vector Pf)
    {
	return vector(random(Pf));
    }

float Cellnoise1D(float t)
    {
	return float(random(t));
    }

#define Fadeout(g, g_avg, featuresize, fwidth)	lerp(g, g_avg, smooth(0.2, 0.6, fwidth/featuresize))
#define Filterednoise(x, w)	Fadeout(noise(x), -2, 2, w)
#define Filteredsnoise(pt, w)	Fadeout(snoise(pt), -2, 2, w)
#define Filteredvnoise(pt, w)	Fadeout(vnoise(pt), 0, 1, w)

/*********************************************************
Noise functions, fractional Brownian motion and Turbulence
*********************************************************/
float Fbm(vector Pf; float filtwidth, octaves, lacunarity, gain)
    {
	float sum = 0.0;
	float amp = 1.0;
	vector pp = Pf;
	float fw = filtwidth;
	float i;
	
	for (i = 0; i < octaves; i++) {
	    sum += amp * Filteredsnoise(pp, fw);
	    amp *= gain;
	    pp *= lacunarity;
	    fw *= lacunarity;
	}
	return sum;
    }


vector Vfbm(vector Pf; float filtwidth, octaves, lacunarity, gain)
    {
	vector sum = {0.0, 0.0, 0.0};
	float amp = 1.0;
	vector pp = Pf;
	float fw = filtwidth;
	float i;
	
	for (i = 0; i < octaves; i++) {
	    sum += amp * Filteredsnoise(pp, fw);
	    amp *= gain;
	    pp *= lacunarity;
	    fw *= lacunarity;
	}
	return sum;
    }


float Turbulence(vector Pf; float octaves, lacunarity, gain)
    {
	float sum = 0.0;
	float amp = 1.0;
	vector pp = Pf;
	float i;
	
	for (i = 0; i < octaves; i++) {
	    sum += amp * abs(snoise(pp));
	    amp *= gain;
	    pp *= lacunarity;
	}
	return sum;
    }


float Turbulence2(vector Pf; float octaves, lacunarity, gain)
    {
	float sum = 0.0;
	float amp = 1.0;
	vector pp = Pf;
	int i;
	
	for (i = 0; i < octaves; i++) {
	    sum += amp * sqrt(pow(snoise(pp), 2));
	    amp *= gain;
	    pp *= lacunarity;
	}
	return sum;
    }


void Voronoi_f1_3d(vector Pf; float jitter; export float f1; export vector pos1)
    {
	vector thiscell = set(floor(Pf.x)+0.5, floor(Pf.y)+0.5, floor(Pf.z)+0.5);
	f1 = 10000.0;
	float i, j, k;
	vector vec1;

	for (i = -1; i <= 1; i++) {
	    for (j = -1; j <= 1; j++) {
		for (k = -1; k <= 1; k++) {
		    vec1 = set(i, j, k);
		    vector testcell = thiscell + vec1;
		    vector pos = testcell + jitter * (Cellnoise3D(testcell) -0.5);
		    vector offset = pos -Pf;
		    float dist = length(offset);
		    if (dist < f1) {
			f1 = dist;
			pos1 =pos;
		    }
		}
	    }
	}
	f1 = sqrt(f1);
    }


void Voronoi_f2_3d(vector Pf; float jitter; export float f1; export vector pos1; export float f2; export vector pos2)
    {
	vector thiscell = set(floor(Pf.x)+0.5, floor(Pf.y)+0.5, floor(Pf.z)+0.5);
	f1 = f2 = 10000.0;
	float i, j, k;
	vector vec1;

	for (i = -1; i <= 1; i++) {
	    for (j = -1; j <= 1; j++) {
		for (k = -1; k <= 1; k++) {
		    vec1 = set(i, j, k);
		    vector testcell = thiscell + vec1;
		    vector pos = testcell + jitter * (Cellnoise3D(testcell) -0.5);
		    vector offset = pos -Pf;
		    float dist = length(offset);
		    if (dist < f1) {
			f2 = f1;
			pos2 = pos1;
			f1 = dist;
			pos1 = pos;
		    } else if ( dist < f2) {
			f2 = dist;
			pos2 =pos;
		    }
		}
	    }
	}
	f1 = sqrt(f1);
    }


float DistToCell(vector Pf; string whichspace; float freq)
    {
	vector pp = ptransform(whichspace, Pf) * freq;
	vector thiscell = set(floor(pp.x)+0.5, floor(pp.y)+0.5, floor(pp.z)+0.5);
	float disttonearest = 10000.0;
	float i, j, k;
	vector vec1;

	for (i = -1; i <= 1; i++) {
	    for (j = -1; j <= 1; j++) {
		for (k = -1; k <= 1; k++) {
		    vec1 = set(i, j, k);
		    vector testcell = thiscell + vec1;
		    float dist = distance(testcell, pp);
		    if (dist < disttonearest)
			disttonearest = dist;
		}
	    }
	}
	
	return disttonearest;
    }


vector Varyeach(vector Cin; float index, varyhue, varysat, varylum)
    {
	vector Chsl = ctransform("cspace:hsl", Cin);
	float hue = getcomp(Chsl, 0);
	float sat = getcomp(Chsl, 1);
	float lum = getcomp(Chsl, 2);
	hue += varyhue * (Cellnoise1D(index + 3) - 0.5);
	sat *= 1 - varysat * (Cellnoise1D(index - 14) - 0.5);
	lum *= 1 - varylum * (Cellnoise1D(index + 37) - 0.5);
	Chsl = set(Mod(hue, 1), clamp(sat, 0, 1), clamp(lum, 0, 1));
	Chsl = clamp(Chsl, {0.0, 0.0, 0.0}, {1.0, 1.0, 1.0});
	return ctransform("cspace:hsl", "cspace:rgb", Chsl);
    }
