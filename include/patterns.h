float Sign(float a)
{
    float sgn;
    if (a == 0) {
	sgn =  0;
    } else if (a > 0) {
	sgn =  1;
    } else {
	sgn = -1;
    }
    return sgn;
}


float Filteredabs(float a, da)
    {
	float a0 = a - 0.5 * da;
	float a1 = a + 0.5 * da;
	return (IntegralAbs(a1) - IntegralAbs(a0)) / da;
    }


float IntegralAbs(float a)
    {
	return Sign(a) * 0.5 * a * a;
    }


float IntegralPulsterain(float a)
    {}


float Mod(float a, b)
    {
	float remainder = a % b;
	if (remainder < 0)
	    remainder += b;
	return remainder;
    }


int Step(float edge, a)
    {
	return a < edge ? 0 : 1;
    }


float Pulse(float edge, edge1, a)
    {
	return Step(edge0, a) - Step(edge1, a);
    }


float Pulsetrain(float edge, period, a)
    {
	return Pulse(edge, period, Mod(a, period));
    }


float FilteredStep(float edge, x, w)
    {
	return clamp((x+w/2-edge)/w, 0, 1);
    }

float FilteredStepArea(float edge, x)
    {
	float w = Du(P) * Dv(P);
	return clamp((x+w/2-edge)/w, 0, 1);
    }

float Integral_FPT(float a, nedge)
    {
	return ((1 - nedge) * floor(a) + max(0, a - floor(a) - nedge));
    }


float FilteredPulse(float edge0, edge1, x, dx)
    {
	float x0 = x - dx/2;
	float x1 = x0 + dx;
	return max(0, ( min(x1, edge1) - max(x0, edge0) ) / dx);
    }


float FilteredPulsetrain(float edge, period, a, da)
    {
	float w = da / period;
	float a0 = a / period - w / 2;
	float a1 = a0 + w;
	float nedge = edge / period;
	return (Integral_FPT(a1, nedge) - Integral_FPT(a0, nedge)) / w;
    }


float SmoothPulse(float e0, e1, e2, e3, a)
    {
	return smooth(e0, e1, a) - smooth(e2, e3, a);
    }


float SmoothPulsetrain(float e0, e1, e2, e3, period, a)
    {
	return SmoothPulse(e0, e1, e2, e3, Mod(a, period));
    }

//rework TilePattern to PlankPattern
float TilePattern(float ss, tt, ds, dt, joint_x, joint_y;
		  export float swhichtile, twhichtile, stile, ttile)
    {
	swhichtile = floor(ss);
	twhichtile = floor(tt);
	stile = ss - swhichtile;
	ttile = tt - twhichtile;
	return FilteredPulsetrain(joint_x, 1, ss + joint_x/2, ds) *
		    FilteredPulsetrain(joint_y, 1, tt + joint_y/2, dt);
    }


float PlankPattern(float ss, tt, dss, dtt, plankwidth, planklength, groovewidth, grooveheight;
		   export float swhichplank, twhichplank, splank, tplank)
    {
	swhichplank = floor(ss / plankwidth);
	splank = ss - swhichplank * plankwidth;
	float newt = tt + planklength * random(swhichplank);
	twhichplank = floor(newt / planklength);
	tplank = newt - twhichplank * planklength;

	return FilteredPulsetrain(groovewidth, plankwidth, ss + groovewidth / 2, dss) +
		FilteredPulsetrain(grooveheight, planklength, newt + groovewidth / 2, dtt);
    }

