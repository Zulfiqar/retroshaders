#include "math.h"

void Spherical_Projection(vector Pf; export float ss, tt, ds, dt)
    {
	vector V = normalize(Pf);
	ss = (-atan(V.z, V.x) + M_PI) / M_PI_2;
	tt = 0.5 - acos(V.y) / M_PI;
	ds = Filterwidth(ss);
	if ( ds > 0.5 )
	    ds = max(1 - ds, MINFILTWIDTH);
	dt = Filterwidth(tt);
	if ( dt > 0.5 )
	    dt = max(1 - dt, MINFILTWIDTH);
    }

void Cylindrical_Projection(vector Pf; export float ss, tt, ds, dt)
    {
	vector V = normalize(Pf);
	//changed to version in Essential Renderman
	ss = atan(V.z, V.x) / M_PI_2; // + 0.5?? toch weer conform AR
	tt = P.z;
	ds = Filterwidth(ss);
	if ( ds > 0.5 )
	    ds = max(1 - ds, MINFILTWIDTH);
	dt = Filterwidth(tt);
    }

void Project_2D(string projection; vector Pf; string whichspace;
		matrix xfrom; export float ss, tt, ds, dt)
    {
	vector Pproj;

	if (projection == "st")
	    Pproj = set(s, t, 0);
	else
	    Pproj = ptransform(whichspace, Pf);
	Pproj = ptransform(Pproj, xfrom);
	if (projection == "planar" || projection == "st") {
	    ss = Pproj.x;
	    tt = Pproj.y;
	    ds = Filterwidth(ss);
	    dt = Filterwidth(tt);
	} 
	if (projection == "perspective") {
	    float y = max(Pproj.y, 1.0e-6);
	    ss = Pproj.x / y;
	    tt = Pproj.z / y;
	    ds = Filterwidth(ss);
	    dt = Filterwidth(tt);
	}
	if (projection == "spherical") {
	    Spherical_Projection(Pproj, ss, tt, ds, dt);
	}
	if (projection == "cylindrical") {
	    Cylindrical_Projection(Pproj, ss, tt, ds, dt);
	}
	/* check if there are uv attributes */
	if (projection == "uv" && isbound("uv")) {
	    Pproj = set(ss, tt, 0);
	    Pproj = ptransform(Pproj, xfrom);
	    ss = Pproj.x;
	    tt = Pproj.y;
	    ds = Filterwidth(ss);
	    dt = Filterwidth(tt);
	}	
    }
