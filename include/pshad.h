/*omit the trainling ; from the last statement since this will be added in the calling program */

#ifndef USE_PREF
  #define USE_PREF 0
#endif

#if (USE_PREF)
    #define UNINITIALIZED_PREF {-1.0e10, -1.0e10, -1.0e10}

    #define PSHAD_PARAMS(spacedefault, freqdefault) \
	    vector Pref = UNINITIALIZED_PREF;	    \
	    string shadingspace = spacedefault;    \
	    float shadingfreq = freqdefault	    \

    #define GET_PSHAD   vector Pshad;				    \
			if (Pref != UNINITIALIZED_PREF)		    \
			    Pshad = ptransform(shadingspace, Pref); \
			else					    \
			    Pshad = ptransform(shadingspace, P);    \
			Pshad *= shadingfreq;			    \
			float dPshad = Filterwidtharea(Pshad)	    \ 
	    		
#else
    #define PSHAD_PARAMS(spacedefault, freqdefault) \
	    string shadingspace = spacedefault;	    \
	    float shadingfreq = freqdefault	    \

    #define GET_PSHAD   vector Pshad;					    \
			Pshad = shadingfreq * ptransform(shadingspace, P);  \
			float dPshad = Filterwidtharea(Pshad)		    \

#endif

#define DEFAULT_PSHAD_PARAMS PSHAD_PARAMS("space:current", 1)
