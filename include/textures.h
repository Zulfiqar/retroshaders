vector4 GetColorTextureAndAlpha (string texturename; string projection; vector Pf;
				string whichspace; matrix xform; float blur;
				float alphachannel; export float alpha;)
    {
	float ss, tt, ds, dt;
	Project_2D(projection, Pf, whichspace, xform, ss, tt, ds, dt);
	ds *= 0.5;
	dt *= 0.5;
	vector4 Ct = texture(texturename, ss-ds, tt-dt, ss+ds, tt-dt, ss-ds, tt+dt, ss+ds, tt+dt, "blur", blur);
	alpha = getcomp(Ct, 3);
	return Ct;
    }

vector ApplyColorTextureOver(vector basecolor; string texturename, projection; vector Pf; 
			    string whichspace; matrix xform; float blur;)
    {
	float alpha;
	vector Ct = vector(GetColorTextureAndAlpha(texturename, projection, Pf, whichspace, xform, blur, 3, alpha));
	return Ct + (1 - alpha)*basecolor;
    }

float ApplyFloatTextureOver(float ks; string texturename, projection; vector Pf; 
			    string whichspace; matrix xform; float blur;)
    {
	float alpha;
	vector Ct = vector(GetColorTextureAndAlpha(texturename, projection, Pf, whichspace, xform, blur, 3, alpha));
	ks = getcomp(Ct, 0);
	/* K-value get's scaled by the alphachannel and the greyvalue of the red channel*/
	ks = clamp(ks, 0, 1);
	return ks * (1 - alpha);
    }


