float TilePattern(float ss, tt, ds, dt, joint_x, joint_y;
		  export float swhichtile, twhichtile, stile, ttile)
    {
	swhichtile = floor(ss);
	twhichtile = floor(tt);
	stile = ss - swhichtile;
	ttile = tt - twhichtile;
	return FilteredPulsetrain(joint_x, 1, ss + joint_x/2, ds) *
		    FilteredPulsetrain(joint_y, 1, tt + joint_y/2, dt);
    }

vector TileTexture(float tileindex, stile, ttile, ds, dt, edgevary, mottling,
		    speckly, mottlefreq; vector Cbase, Cedge, Cmottle, Cspeck)
    {
	vector C = Cbase;
	float dst = max(ds, dt);
	vector noisep;
	if (mottling > 0) {
	    noisep = mottlefreq * set(stile, ttile, tileindex);
	    float mottle = 0.2 + 0.6 * Fbm(noisep, mottlefreq * dst, 4, 2, 0.65);
	    C = lerp(C, Cmottle, clamp(mottling * mottling, 0, 1));
	}
	if (edgevary > 0) {
	    float sedgeoffset = 0.05 * Fbm(set(stile * 10, ttile * 10, tileindex + 10), 10 * dst, 2, 2, 0.5);
	    float tedgeoffset = 0.05 * Fbm(set(stile * 10, ttile * 10, tileindex -  3), 10 * dst, 2, 2, 0.5);
	    float edgy = 1 - (SmoothPulse(.05, 0.15, 0.85, 0.95, stile + sedgeoffset) *
				SmoothPulse(.05, 0.15, 0.85, 0.95, ttile + tedgeoffset));
	    C = lerp(C, Cedge, edgevary * edgy);
	}
	if (speckly > 0) {
	    float speckfreq = 7;
	    noisep = set(stile * speckfreq, ttile * speckfreq, tileindex + 8);
	    float specky = Filteredsnoise(noisep, speckfreq * dst);
	    specky = smooth(0.55, 0.7, specky);
	    C = lerp(C, Cspeck, speckly * specky);
	}
	return C;  
    }	

vector Varyeach(vector Cin; float index, varyhue, varysat, varylum)
    {
	vector Chsl = ctransform("cspace:hsl", Cin);
	float hue = getcomp(Chsl, 0);
	float sat = getcomp(Chsl, 1);
	float lum = getcomp(Chsl, 2);
	hue += varyhue * (Cellnoise1D(index + 3) - 0.5);
	sat *= 1 - varysat * (Cellnoise1D(index - 14) - 0.5);
	lum *= 1 - varylum * (Cellnoise1D(index + 37) - 0.5);
	Chsl = set(Mod(hue, 1), clamp(sat, 0, 1), clamp(lum, 0, 1));
	Chsl = clamp(Chsl, {0.0, 0.0, 0.0}, {1.0, 1.0, 1.0});
	return ctransform("cspace:hsl", "cspace:rgb", Chsl);
    }
