/*User Interface*/

/*Main tab*/
#pragma label tilesize_x "Tilesize in X"
#pragma label tilesize_y "Tilesize in Y"
#pragma label jointwidth_x "Joint Width in X"
#pragma label jointwidth_y "Joint Width in Y"
#pragma label jointdepth "Depth of Joint"
#pragma label Krand "Scale Random Noise"
#pragma label projection "Texture Projection"
#pragma label whichspace "Shading Space"
#pragma label trl "Translate"
#pragma label rot "Rotate"
#pragma label scl "Scale"
#pragma label pvt "Pivot"

/*Displacement map tab*/
#pragma label Dsmapname "Displacement Map"
#pragma label filter "Texture Filter"
#pragma label width "Filter Width"
#pragma label mapblur "Blur Texture"
#pragma label wmode "Wrap mode"
#pragma label Km "Displacement Intensity"
#pragma label mdisp "Minimum Displacement"
#pragma label use_alpha "Use Alpha Mask"
#pragma label over "Composite Over"
#pragma label trs "Transform Order"
#pragma label ctrl_x "Translate"
#pragma label ctrl_y " "
#pragma label crot_z "Rotate"
#pragma label cscl_x "Scale"
#pragma label cscl_y " "
#pragma label cpvt_x "Pivot"
#pragma label cpvt_y " "

/*No tab*/
#pragma label truedisp "Bump Displacement"
#pragma label fixpoly "Fix Polygons"

/*Group*/
#pragma group "Base" tilesize_x tilesize_y jointwidth_x jointwidth_y jointdepth Krand projection whichspace trl rot scl pvt
#pragma group "Displacement Map" Dsmapname filter width mapblur wmode  Km mdisp use_alpha over 
#pragma group "Displacement Map" trs ctrl_x ctrl_y crot_z cscl_x cscl_y cpvt_x cpvt_y

/*Pragma hints*/
#pragma choice projection "none" "None"
#pragma choice projection "st" "st"
#pragma choice projection "uv" "uv"
#pragma choice projection "planar" "Planar"
#pragma choice projection "perspective" "Perspective"
#pragma choice projection "spherical" "Spherical"
#pragma choice projection "cylindrical" "Cylindrical"
#pragma choice whichspace "space:current" "current"
#pragma choice whichspace "space:world" "world"
#pragma choice whichspace "space:camera" "camera"
#pragma choice whichspace "space:object" "object"
#pragma choice whichspace "space:texture" "texture"
#pragma choice whichspace "space:ndc" "ndc"
#pragma hint trl vector
#pragma hint rot vector
#pragma hint scl vector
#pragma hint pvt vector
#pragma hint Dsmapname image
#pragma hint use_alpha toggle
#pragma choice filter "point" "point"
#pragma choice filter "box" "box"
#pragma choice filter "gauss" "gauss"
#pragma choice filter "bartlett" "bartlett"
#pragma choice filter "sinc" "sinc"
#pragma choice filter "hanning" "hanning"
#pragma choice filter "blackman "blackman"
#pragma choice filter "catrom" "catrom"
#pragma range width 0 4.0
#pragma range mapblur !0 1.0
#pragma choice wmode "" "default"
#pragma choice wmode "repeat" "repeat"
#pragma choice wmode "clamp" "clamp"
#pragma choice wmode "decal" "decal"
#pragma choice trs "0" "Scale, Rotate, Translate"
#pragma choice trs "1" "Scale, Translate, Rotate"
#pragma choice trs "2" "Rotate, Scale, Translate"
#pragma choice trs "3" "Rotate, Translate, Scale"
#pragma choice trs "4" "Translate, Scale, Rotate"
#pragma choice trs "5" "Translate, Rotate, Scale"
#pragma hint ctrl_x joinnext
#pragma hint cscl_x joinnext
#pragma hint cpvt_x joinnext
#pragma hint over toggle
#pragma hint truedisp toggle
#pragma hint fixpoly toggle

/*Importable attributes*/
#pragma hint dimage hidden
#pragma hint vary hidden
#pragma hint uv hidden
#pragma hint uv vector
