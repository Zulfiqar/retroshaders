/*User Interface*/

/*Base tab*/
#pragma label tilesize_x "Number of Tiles in X"
#pragma label tilesize_y "Number of Tiles in Y"
#pragma label jointwidth_x "Joint Width in X"
#pragma label jointwidth_y "Joint Width in Y"
#pragma label Cbase "Base Colour"
#pragma label Cjoint "Joint Colour"
#pragma label Ka "Ambient Intensity"
#pragma label Kdmortar "Diffuse Intensity Mortar"
#pragma label Kdtile "Diffuse Intensity Tile"
#pragma label Ks "Specular Intensity"
#pragma label Kr "Reflective Intensity"
#pragma label roughness "Roughness"
#pragma label seed_mortar "Seed Mortar Noise"
#pragma label eta "Refraction Index"
#pragma label projection "Texture Projection"
#pragma label whichspace "Shading Space"
#pragma label trl "Translate"
#pragma label rot "Rotate"
#pragma label scl "Scale"
#pragma label pvt "Pivot"

/*Noise tab*/
#pragma label Cedge "Edge Colour"
#pragma label Cmottle "Mottle Colour"
#pragma label Cspeck "Speckle Colour"
#pragma label edgevary "Vary Edge"
#pragma label mottling "Mottling"
#pragma label speckly  "Speckle Amount"
#pragma label mottlefreq "Mottle Frequency"
#pragma label varyhue "Vary Hue"
#pragma label varysat "Vary Saturation"
#pragma label varylum "Vary Lumminance"
#pragma label specsharpness "Speckle Sharpness"
#pragma label blur "Blur Amount"

/*Imagemap tab*/
#pragma label Csmapname "Basecolor Map"
#pragma label filter "Texture Filter"
#pragma label width "Filter Width"
#pragma label mapblur "Blur Texture"
#pragma label wmode "Wrap mode"
#pragma label trs "Transform Order"
#pragma label ctrl_x "Translate"
#pragma label ctrl_y " "
#pragma label crot_z "Rotate"
#pragma label cscl_x "Scale"
#pragma label cscl_y " "
#pragma label cpvt_x "Pivot"
#pragma label cpvt_y " "

/*Environment tab*/
#pragma label envname "Area Map"
#pragma label envspace "Space"
#pragma label envrad "Radius of Hemisphere"

/*No tab*/
#pragma label facefront "Face Normals"


/*Group in tabs*/
#pragma group "Base" tilesize_x tilesize_y jointwidth_x jointwidth_y Cbase Cjoint Ka Kdmortar
#pragma group "Base" Kdtile Ks Kr roughness seed_mortar eta projection whichspace trl rot scl pvt
#pragma group "Noise" Cedge Cmottle Cspeck edgevary mottling speckly mottlefreq varyhue varysat varylum specsharpness blur
#pragma group "Image Map" Csmapname filter width mapblur wmode trs ctrl_x ctrl_y crot_z cscl_x cscl_y cpvt_x cpvt_y
#pragma group "Environment" envname envspace envrad


/*Pragma hints, ranges and menus*/
#pragma hint Cbase color
#pragma hint Cjoint color
#pragma range Ka	!0 1
#pragma range Kdmortar	!0 1
#pragma range Kdtile	!0 1
#pragma range Ks	!0 1
#pragma range Kr	!0 1
#pragma choice projection "st" "st"
#pragma choice projection "uv" "uv"
#pragma choice projection "planar" "Planar"
#pragma choice projection "perspective" "Perspective"
#pragma choice projection "spherical" "Spherical"
#pragma choice projection "cylindrical" "Cylindrical"
#pragma choice whichspace "space:current" "current"
#pragma choice whichspace "space:world" "world"
#pragma choice whichspace "space:camera" "camera"
#pragma choice whichspace "space:object" "object"
#pragma choice whichspace "space:texture" "texture"
#pragma choice whichspace "space:ndc" "ndc"
#pragma hint trl vector
#pragma hint rot vector
#pragma hint scl vector
#pragma hint pvt vector
#pragma hint Cedge color
#pragma hint Cmottle color
#pragma hint Cspeck color
#pragma range edgevary	!0 1
#pragma range mottling	!0 1
#pragma range speckly	!0 1
#pragma range varyhue	!0 1
#pragma range varysat	!0 1
#pragma range varylum	!0 1
#pragma range roughness !0 1
#pragma range eta       !0 3
#pragma range blur      !0 1
#pragma hint Csmapname image
#pragma choice filter "point" "point"
#pragma choice filter "box" "box"
#pragma choice filter "gauss" "gauss"
#pragma choice filter "bartlett" "bartlett"
#pragma choice filter "sinc" "sinc"
#pragma choice filter "hanning" "hanning"
#pragma choice filter "blackman "blackman"
#pragma choice filter "catrom" "catrom"
#pragma range width     0  4
#pragma range mapblur   !0 1
#pragma choice wmode "" "default"
#pragma choice wmode "repeat" "repeat"
#pragma choice wmode "clamp" "clamp"
#pragma choice wmode "decal" "decal"
#pragma choice trs "0" "Scale, Rotate, Translate"
#pragma choice trs "1" "Scale, Translate, Rotate"
#pragma choice trs "2" "Rotate, Scale, Translate"
#pragma choice trs "3" "Rotate, Translate, Scale"
#pragma choice trs "4" "Translate, Scale, Rotate"
#pragma choice trs "5" "Translate, Rotate, Scale"
#pragma hint ctrl_x joinnext
#pragma hint cscl_x joinnext
#pragma hint cpvt_x joinnext
#pragma hint envname image
#pragma choice envspace   "space:current" "current"
#pragma choice envspace   "space:world" "world"
#pragma choice envspace   "space:camera" "camera"
#pragma choice envspace   "space:object" "object"
#pragma choice envspace   "space:texture" "texture"
#pragma choice envspace   "space:ndc" "ndc"
#pragma hint facefront toggle



/*Importable attributes*/
#pragma hint uv hidden
#pragma hint uv vector
#pragma hint Cd hidden
#pragma hint Cd color
#pragma hint vary hidden
#pragma hint cimage hidden
#pragma hint cimage image


/*export parameters*/
#pragma hint Ctile invisible
#pragma hint Cmortar invisible
#pragma hint exportDiffuse invisible
#pragma hint exportAmbient invisible
#pragma hint exportSpec    invisible
