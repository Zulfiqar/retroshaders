#define USE_PREF 0

#include "../pragma/oakplank_pragma.h"
#include "../include/filter.h"
#include "../include/project.h"
#include "../include/noise.h"
#include "../include/materials.h"
#include "../include/environment.h"
#include "../include/local_illumination.h"
#include "../include/lighting_models.h"
#include "../include/patterns.h"
#include "../include/displace.h"
#include "../include/pshad.h"

surface retro_oakplank (float Ka = 0.0;
		  float Kd = 1.0;
		  float Ks = 0.75;
		  float Kr = 1.0;
		  float roughness = 0.1;
		  float blur = 0.0;
		  float eta = 1.5;
		  DEFAULT_PSHAD_PARAMS;
		  float ringfreq = 8.0;
		  float ringunevenness = 0.5;
	          float ringnoise = 0.02;
		  float ringnoisefreq = 1.0;
	          float grainfreq = 25.0;
		  float trunkwobble = 0.15;
	          float trunkwobblefreq = 0.025;
		  float angularwobble = 1.0;
	          float angularwobblefreq = 1.5;
		  float divotdepth = 0.05;
	          vector Clightwood = {0.5, 0.2, 0.067};
		  vector Cdarkwood = {0.15, 0.077, 0.028};
		  vector Cgroove = {0.02,0.02, 0.02};
	          float ringy = 1.0;
		  float grainy = 1.0;
		  float plankwidth = 2.0;
		  float planklength = 30.0;
		  float groovewidth = 0.05;
		  float grooveheight = 0.05;
		  float groovedepth = 0.03;
		  float edgewidth = 0.1;
		  DECLARE_DEFAULT_ENVPARAMS;
		  float varyhue = 0.015;
		  float varysat = 0.1;
		  float varylum = 0.5;
		  float varnishlump = 0.01;
		  float varnishlumpfreq = 0.5;
	          int truedisp = 1;
		 )
    {
	GET_PSHAD;
	float ss = getcomp(Pshad, 0);
	float tt = getcomp(Pshad, 2);
	float height = getcomp(Pshad, 1);
	float dss = Filterwidth(ss);
	float dtt = Filterwidth(tt);

	float swhichplank, twhichplank, splank, tplank;
	float inplank = PlankPattern(ss, tt, dss, dtt, plankwidth, planklength, groovewidth,
				     grooveheight, swhichplank, twhichplank, splank, tplank);
	//check tiles for a better solution
	float plankindex = swhichplank + twhichplank;
	
	//vector v1 = set(splank -0.5, height - 0.01 * tplank, tplank);
	//vector v2 = set(1.0, 5.0, 10.0);
	//vector v3 = vector(random(swhichplank, twhichplank)) -0.5;
	vector Ppat = set(splank -0.5, height - 0.01 * tplank, tplank) + {1, 5, 10.0} * (vector(random(swhichplank, twhichplank)) -0.5) ;
	//vector Ppat =  {splank - 0.5, height - 0.01 * tplank, tplank} + 
	//		    {1.0, 5.0, 10.0} * (vector random(swhichplank, twhichplank) - 0.5);
	float wood = OakTexture(Ppat, dPshad, ringfreq, ringunevenness, grainfreq, ringnoise,
				ringnoisefreq, trunkwobble, trunkwobblefreq, angularwobble,
				angularwobblefreq, ringy, grainy);
	vector Cwood = lerp(Clightwood, Cdarkwood, wood);
	Cwood = Varyeach(Cwood, plankindex, varyhue, varysat, varylum);
	Cwood = lerp(Cgroove, Cwood, inplank);

	//why can't Houdini do displacement inside surface shaders
	//like Renderman? 
    
	float edgedisp = SmoothPulse(0, edgewidth, plankwidth - edgewidth, plankwidth, splank);
	edgedisp *= SmoothPulse(0, edgewidth, planklength - edgewidth, planklength, tplank);
	vector Nf = frontface(normalize(N), I);
	float disp = -wood * divotdepth + groovedepth * (edgedisp -1);
	vector Pt = P;
	disp += varnishlump + Filteredsnoise(Pshad * varnishlumpfreq, dPshad * varnishlumpfreq);
	Nf = frontface(Displacevector(Nf, "space:texture", disp, truedisp, Pt), I);

	float specadjusted = 1 + 0.3 * wood - 0.8 * (1 - inplank);
	Cf = MaterialPlasticShiny(Pt, Nf, Cwood, Ka, Kd, specadjusted * Ks, specadjusted * Kr,
				    roughness, eta, blur, ENVPARAMS);
	Cf *= Of;
    }
