/*listing 8.1 from Advanced Renderman, pg. 184*/

surface paintedplastic (
	float Ka = 1.0;
	float Kd = 0.5;
	float Ks = 0.1;
	float roughness = 0.1;
	color specularcolor = 1;
	string texturename = "";
)

{
	color Ct = Cs;
	if (texturename != "")
		Ct *= color texture (texturename);
	normal Nf = faceforward(normalize(N),I);
	vector V = -normalize(I);
	Ci = Ct * (Ka * ambient() + Kd * diffuse(Nf)) + specularcolor * Ks * specular(Nf, V, roughness);
	Oi = Os;
	Ci *= Oi;
}
