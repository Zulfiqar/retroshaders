surface retro_rust(
    uniform float Ks = 0.4;
    uniform float Kd = 0.6;
    uniform float Ka = 0.0;
    varying float roughness = 0.2;
)
{
    vector Nn = normalize(N);
    vector In = normalize(I);
    vector V  = normalize(faceforward(I, Nn));
    Ci = Cs * (Kd * diffuse(Nn) + Ks * specular(Nn, V, roughness) + Ka * ambient());
}
