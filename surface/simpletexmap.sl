/*listing 8.2 from Advanced Renderman, pg. 187*/

surface simpletexmap (
	float Ka = 1.0, Kd = 1.0, Ks = 0.1;
	float roughness = 0.1;
	color specularcolor = 1;
	string texturename = "";
	float sstart = 0, sscale = 1;
	float tstart = 0, tscale = 1;
)

{
	float ss = (s - sstart) / sscale;
	float tt = (t - tstart) / tscale;

	color Ct;
	if ( texturename != "") {
		float opac = float texture (texturename[3], ss, tt);
		Ct = color texture (texturename, ss, tt) + (1-opac)*Cs;
	} else
		Ct = Cs;

	normal Nf = faceforward(normalize(N), I);
	vector V = -normalize(I);
	Ci = Ct * (Ka * ambient() + Kd * diffuse(Nf)) + Ks * specularcolor * specular(Nf, V, roughness);
	Oi = Os;
	Ci *= Oi;
}
