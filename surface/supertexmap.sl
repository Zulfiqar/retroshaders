#include "../include/alias.h"
#include "../include/matrix.h"
#include "../include/projection.h"
#include "../include/displace.h"
#include "../include/color.h"

surface supertexmap (
	float Ka = 1;
	float Kd = 0.5;
	float Ks = 0.5;
	float roughness = 0.1;
	color specularcolor = 1;
	float blur = 0;
	/* basecolor */
	string Csmapname = "";
	string Csproj = "st";
	string Csspace = "shader";
	float Csmx[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
	/* opacity */
	string Osmapname = "";
	string Osproj = "st";
	string Osspace = "shader";
	float Osmx[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
	/* specularity */
	string Ksmapname = "";
	string Ksproj = "st";
	string Ksspace = "shader";
	float Ksmx[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
	/* displacement */
	string Dsmapname = "";
	string Dsproj = "st";
	string Dsspace = "shader";
	float Dsmx[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
	float truedisp = 1;
)
{
	color Ct = Cs;
	color Ot = Os;
	float ks = Ks;
	float disp = 0;
	
	normal Nf = faceforward(normalize(N), I);
	vector Vf = -normalize(I);
	
	if (Csmapname != "")
		Ct = ApplyColorTextureOver(Ct, Csmapname, Csproj, P, Csspace, array_to_mx(Csmx), blur);
	if (Osmapname != "")
		Ot = ApplyColorTextureOver(Ct, Osmapname, Osproj, P, Osspace, array_to_mx(Osmx), blur); 
	if (Ksmapname != "") 
		ks = ApplyFloatTextureOver(Ksmapname, Ksproj, P, Ksspace, array_to_mx(Ksmx), blur); 
	if (Dsmapname != "") {
		disp += ConvertToGrey(Dsmapname, Dsproj, P, Dsspace, array_to_mx(Dsmx), blur);
		N = Displace(normalize(N), Dsspace, disp, truedisp);
	}
	
	/* plastic illumination model */
	Ci = Ct * (Ka*ambient() + Kd*diffuse(Nf)) + specularcolor * ks * specular(Nf, Vf, roughness);
	Oi = Ot;
	Ci *= Oi;
}
