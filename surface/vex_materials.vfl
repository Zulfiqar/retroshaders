#define VOP_SHADING
#define VOP_SURFACE

#pragma opname retro_materials
#pragma oplabel "Retro Materials"

#pragma label mat "Material"
#pragma choice mat "Plastic" "Plastic"
#pragma choice mat "Matte" "Matte"
#pragma choice mat "Metal" "Metal (rough)"
#pragma choice mat "Tplastic" "Thin Plastic"
#pragma label Ka "Ambient"
#pragma label Kd "Diffuse"
#pragma label Ks "Specular"
#pragma label Kt "Translucency"
#pragma hint basecolor color
#pragma label basecolor "Specular color"
#pragma label roughness "Roughness"

vector MaterialPlastic (vector Nf, basecolor; float Ka, Kd, Ks, roughness)
    {
	return basecolor * ( Ka * ambient() + Kd *diffuse(Nf)) + Ks * specular(Nf, -normalize(I), roughness);
    }

vector MaterialMatte (vector Nf, basecolor; float Ka, Kd)
    {
	return basecolor * ( Ka * ambient() + Kd * diffuse(Nf));
    }

vector MaterialRoughMetal (vector Nf, basecolor; float Ka, Kd, Ks, roughness)
    {
	return basecolor * ( Ka * ambient() + Kd *diffuse(Nf) + Ks * specular(Nf, -normalize(I), roughness) );
    }

vector MaterialThinPlastic (vector Nf, Nr, V, basecolor; float Ka, Kd, Ks, Kt, roughness)
    {
	return basecolor * ( Ka * ambient() + Kd * diffuse(Nf) + Kt * diffuse(Nr) ) + Ks * specular(Nf, V, roughness);
    }


surface retro_materials (string mat = "Plastic"; float Ka = 1.0, Kd = 0.8, Ks = 0.5, Kt = 0.2, roughness = 0.1; vector basecolor = {1,1,1};)
    {
	vector Nf = frontface(normalize(N), I);
	vector V = -normalize(I);
	
	if ( mat == "Plastic" ) {
	    Cf = MaterialPlastic(Nf, basecolor, Ka, Kd, Ks, roughness);
	} else if ( mat == "Matte" ) {
	    Cf = MaterialMatte(Nf, basecolor, Ka, Kd);
	} else if ( mat == "Metal" ) {
	    Cf = MaterialRoughMetal(Nf, basecolor, Ka, Kd, Ks, roughness);
	} else if ( mat == "Tplastic" ) {
	    Cf = MaterialThinPlastic(Nf, N, V, basecolor, Ka, Kd, Ks, Kt, roughness);
	}

	vector Oi = Of;
	Cf *= Oi;
    }
