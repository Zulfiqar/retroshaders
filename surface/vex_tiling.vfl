#pragma opname retro_tiling
#pragma oplabel "Retro Tilings"

#pragma label Ka "Ambient Intensity"
#pragma label Kd "Diffuse Intensity"
#pragma label texturename "Texture Map"
#pragma label wmode "Wrap Mode"
#pragma label sstart "Offset S"
#pragma label tstart "Offset T"
#pragma label sscale "Scale S"
#pragma label tscale "Scale T"
#pragma label blur "Blur"
#pragma label filter "Texture Filter"
#pragma label width "Filterwidth"
#pragma label count "Number of tiles"
#pragma label over "Composite Over"
#pragma label basecolor "Base Color"

#pragma hint texturename image
#pragma choice filter "point" "point"
#pragma choice filter "box" "box"
#pragma choice filter "gauss" "gauss"
#pragma choice filter "bartlett" "bartlett"
#pragma choice filter "sinc" "sinc"
#pragma choice filter "hanning" "hanning"
#pragma choice filter "blackman "blackman"
#pragma choice filter "catrom" "catrom"
#pragma label wmode "Wrap mode"
#pragma choice wmode "repeat" "repeat"
#pragma choice wmode "periodic" "periodic"
#pragma choice wmode "clamp" "clamp"
#pragma choice wmode "edge" "edge"
#pragma choice wmode "black" "black"
#pragma choice wmode "decal" "decal"
#pragma hint over toggle
#pragma hint basecolor color

surface retro_tiling(
		 float Ka = 0.5;
		 float Kd = 1.7;
		 string texturename = "";
		 string wmode = "clamp";
		 float sstart = 0.0;
		 float tstart = 0.0;
		 float sscale = 1.0;
		 float tscale = 1.0;
		 float blur = 0;
		 string filter = "gauss";
   		 float width = 1;
		 float count = 5;
		 int over = 0;
		 vector basecolor = {1.0, 1.0, 1.0};
		)
    {
	float stile = floor(s * count);
	float sramp = (s * count) % 1;
	float ttile = floor(t * count);
	float tramp = (t * count) % 1;

	sramp = (sramp - sstart) / sscale;
	tramp = (tramp - tstart) / tscale;

	vector4 Ct = 0;
	float alpha = 0;
	if ( texturename != "" ) {
	    Ct = texture(texturename, sramp, tramp, "pixelblur", blur, "filter", filter, "width", width, "wrap", wmode);
	    alpha = Ct.a;
	}
 
	float result = (stile + ttile + 2) % 2;
 
	vector Nf = frontface(normalize(N), I);
	vector Cs = basecolor * (1 - result - sramp + 2 * sramp * result);
	Cf = Cs * (Ka * ambient() + Kd *diffuse(Nf));
	Cf = Ct + (1 - over) * (1 - alpha) * Cf;

	Cf *= Of;	

    }

